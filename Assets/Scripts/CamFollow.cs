using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
   private Vector3 offset;
   [SerializeField] private Transform target;
   private Vector3 _cuurentVelocity = Vector3.zero;

   private void Awake()
   {
      offset = transform.position - target.transform.position;
   }

   private void Update()
   {
      Vector3 targetPos = target.position + offset;
      transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref _cuurentVelocity,0);
   }
}
