using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

public class Pae : MonoBehaviour
{
    // Start is called before the first frame update
    protected bool _BoostSpeed = false;
    private int _health = 3;
    float speedtime;
    [SerializeField] private Animator playerAnim;
    [SerializeField]private Rigidbody playerRigid;
    private float w_speed, wb_speed, ro_speed;
    [SerializeField]private bool walking;
    [SerializeField]private Transform playerTrans;
    private bool Wpress;
    private bool Apress;
    private bool Spress;
    private bool Dpress;
    public int Health
    {
        get => _health;
        set
        {
            _health = value;
            if (_health <= 0)
                _health = 0;

        }
    }

    private void Start()
    {
        w_speed = 800;
        wb_speed = 500;
        ro_speed = 200;
        
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            playerTrans.localRotation = Quaternion.Euler(0, 0, 0);
            Wpress = true;
        }

        if (Input.GetKey(KeyCode.S))
        {
            playerTrans.localRotation = Quaternion.Euler(0, 180, 0);
            Spress = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            playerTrans.localRotation = Quaternion.Euler(0, -90, 0);
            Apress = true;
        }

        if (Input.GetKey(KeyCode.D))
        {
            playerTrans.localRotation = Quaternion.Euler(0, 90, 0);
            Dpress = true;
        }
        if (Apress&&Wpress)
        {
            playerTrans.localRotation = Quaternion.Euler(0, -45, 0);
            playerRigid.velocity = transform.forward * w_speed * Time.deltaTime;
        }
        if (Dpress&&Wpress)
        {
            playerTrans.localRotation = Quaternion.Euler(0, 45, 0);
            playerRigid.velocity = transform.forward * w_speed * Time.deltaTime;
        }
        if (Apress&&Spress)
        {
            playerTrans.localRotation = Quaternion.Euler(0, -135, 0);
            playerRigid.velocity =  transform.forward * w_speed * Time.deltaTime;
        }
        if (Spress&&Dpress)
        {
            playerTrans.localRotation = Quaternion.Euler(0, 135, 0);
            playerRigid.velocity =  transform.forward * w_speed * Time.deltaTime ;
        }
        else if (Wpress || Apress || Spress || Dpress)
        {
            playerRigid.velocity = transform.forward * w_speed * Time.deltaTime;
        }
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.W)) 
        {
            Wpress = false;
        }
        if (Input.GetKeyUp(KeyCode.A)) 
        {
            Apress = false;
        }
        if (Input.GetKeyUp(KeyCode.S)) 
        {
            Spress = false;
        }
        if (Input.GetKeyUp(KeyCode.D)) 
        {
            Dpress = false;
        }
        
        
         
        
        if ((Dpress&&Spress&&Apress&&Wpress) == false)
        {
            playerRigid.isKinematic = true;
            playerRigid.velocity = Vector3.zero;
            playerAnim.SetBool("Idle",true);
            playerAnim.SetBool("walk",false);
        }
        if (Dpress||Spress||Apress||Wpress)
        {
            playerRigid.isKinematic = false;
            playerAnim.SetBool("walk",true);
            playerAnim.SetBool("Idle",false);
            
        }
        
        if (_health == 0)
        {
            _health = 0;
            Die();
        }
        
        if (_BoostSpeed == true)
        {
            speedtime += Time.deltaTime;
            if (speedtime > 7)
            {
                _BoostSpeed = false;
                w_speed = 800;
                wb_speed = 500;
                speedtime = 0;
            }
        }
    }
    
    public void SetSpeed(float NewSpeed)
    {
        _BoostSpeed = true;
        w_speed = NewSpeed; 
        wb_speed = NewSpeed - 300;
        speedtime = 0;

    }

    public void Die()
    {
       
        Destroy(gameObject);
    }
}
