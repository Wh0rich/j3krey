using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField] private GameObject EnemyMeleePrefabs;
    [SerializeField] private GameObject EnemyShootPrefabs;
    //[SerializeField] private GameObject EnemyMeleePrefabs;
    [SerializeField] private Camera mainCamera;
    

    void Start()
    {
        
        StartCoroutine(CountTime());
    }

   
    void SpawnMelee()
    {
        Vector3 SpawnPos;
        Vector3 PlayerPos;
        var player = FindObjectOfType<Pae>();
        PlayerPos = player.transform.position;
        SpawnPos = new Vector3(Random.Range(-5, 5), 0.5f,Random.Range(-5,5)) * 5;
        SpawnPos += PlayerPos;
        GameObject go = Instantiate(EnemyMeleePrefabs);
        go.transform.position = SpawnPos;

    }
    void SpawnShoot()
    {
        Vector3 SpawnPos;
        Vector3 PlayerPos;
        var player = FindObjectOfType<Pae>();
        PlayerPos = player.transform.position;

        SpawnPos = new Vector3(Random.Range(-5, 5), 0.5f,Random.Range(-5,5)) * 5;
        SpawnPos += PlayerPos;
        
        GameObject go = Instantiate(EnemyShootPrefabs);
        go.transform.position = SpawnPos;

    }

    private IEnumerator CountTime()
    {
        while (GameTimer.Currenttime >= 0)
        {
            yield return new WaitForSeconds(1f);
            
            switch (GameManager.Wave)
            {
                case 1 : if(GameTimer.Currenttime%5 ==0)
                        SpawnMelee();
                    if(GameTimer.Currenttime%10 ==0)
                        SpawnShoot();
                    break;
                case 2 : if(GameTimer.Currenttime%4 ==0)
                        SpawnMelee();
                    if(GameTimer.Currenttime%10 ==0)
                        SpawnShoot();
                    break;
                case 3 : if(GameTimer.Currenttime%4 ==0)
                        SpawnMelee();
                    if(GameTimer.Currenttime%10 ==0)
                        SpawnShoot();
                    break;
                case 4 :  if(GameTimer.Currenttime%3 ==0)
                        SpawnMelee();
                    if(GameTimer.Currenttime%8 ==0)
                        SpawnShoot();
                    break;
                
            }
           
           
        }
    }
}
