using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public class Bullet : MonoBehaviour
{
    protected float m_Mass = 0.5f;
    protected float m_ForceMagnitude = 1f;
    protected float m_LaunchInterval = 0;
    protected float m_LifeTime = 5;
    private Pae player;
    void Start()
    {
        player = FindObjectOfType<Pae>();
    }

    // Update is called once per frame
    void Shoot()
    {
        Rigidbody rb = this.gameObject.GetComponent<Rigidbody>(); 
        rb.mass = m_Mass;
        rb.AddForce(this.transform.forward* m_ForceMagnitude, ForceMode.Impulse);
        

    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            player.Health -= 1;
            other.rigidbody.velocity = Vector3.zero;
        }
        Destroy(gameObject);
    }
    
}
