using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Shoot : MonoBehaviour
{
    private Pae player;
    [SerializeField] private GameObject ShootArea;
    [SerializeField]private BulletSpawn BS = new BulletSpawn();
    private float Speed = 0.03f;

    private void Start()
    {
        
        //ShootArea.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        player = FindObjectOfType<Pae>();
        if (BS.InRange())
            Speed = 0;
        if (!BS.InRange())
            Speed = 0.03f;
        transform.LookAt(player.transform);
       transform.position = Vector3.MoveTowards(transform.position,player.transform.position,Speed);

      
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Bullet")
        {
           
        }
    }
    public void Stop()
    {
        DestroyImmediate(gameObject);
    }
}
