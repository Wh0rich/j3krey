using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Melee : MonoBehaviour
{
    private float Speed = 0.04f;
    private float _coolDownduration = 3f;
    private bool canAttack;
    void Start()
    {
        canAttack = true;
        switch (GameManager.Wave)
        {
            case 2 : Speed += 0.005f;
                break;
            case 3 : Speed += 0.005f;
                break;
            case 4 : Speed += 0.005f;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        Pae player = FindObjectOfType<Pae>();
        transform.LookAt(player.transform);
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, Speed);
        if (!canAttack)
            CoolDown();
        
       
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Player"))
        {
            if (canAttack)
            {
                Attack();
                canAttack = false;
            }
        }
    }
    private void OnCollisionStay(Collision other) //ถ้าแช่เกิน 3 วิโดนตีอีก
    {
        if (other.collider.CompareTag("Player"))
        {
            if (canAttack)
            {
               Attack();
               canAttack = false;
            }
        }
    }
    
    float time;
    void CoolDown()
    {
        if (GameTimer.Currenttime == time)
        {
            canAttack = true;
        }
    }
    void Attack()
    {
        Pae player = FindObjectOfType<Pae>();
        player.Health -= 1;
        time = GameTimer.Currenttime + _coolDownduration;
    }

    public void Stop()
    {
        DestroyImmediate(gameObject);
    }
}
