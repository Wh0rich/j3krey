using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class Meteo : MonoBehaviour
{
    private Vector3 m_Speed;
    [SerializeField] private GameObject Meteor;
    [SerializeField] private GameObject Waypoint;
    [SerializeField] private float m_DesireSpeedMagnitude=0.02f;
    private bool spawn,spawnWay;
    private Vector3 waypoint;
    private Vector3 playerPos;
    private float timer;
    
  
    // Start is called before the first frame update
    void Start()
    {
        spawnWay = true;
        
    }

    private void FixedUpdate()
    {
        if ( spawnWay == true)
        {
            var player = FindObjectOfType<Pae>();
            spawnWay = false;
            playerPos = player.transform.position;
            waypoint = new Vector3(playerPos.x + Random.Range(-5, 5), 0.5f, playerPos.z + Random.Range(-5,5));
            Waypoint.transform.position = waypoint;
            Waypoint = GameObject.Instantiate(Waypoint);
            spawn = true;
        }

        if (spawn == true)
        {
            Meteor.transform.position = Vector3.MoveTowards( Meteor.transform.position, Waypoint.transform.position, 0.5f);
        }
    }


   
    private void OnCollisionEnter(Collision other)
    {
        Explode();
    }
    protected void Explode()
    {
        float m_ImpulsePower = 50;
        float m_Radius = 5f;
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, m_Radius);
        foreach (Collider nearbyObject  in colliders)
        {
            if (nearbyObject.CompareTag("Player"))
            {
                var player = FindObjectOfType<Pae>();
                player.Health -= 1;
            }
            if (nearbyObject.CompareTag("Enemy"))
            {
                Destroy(nearbyObject.gameObject);
            }
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(m_ImpulsePower, transform.position, m_Radius);
            }
        }
        Destroy(Meteor, 0);
        Destroy(Waypoint);
        spawnWay = true;
    }

}
