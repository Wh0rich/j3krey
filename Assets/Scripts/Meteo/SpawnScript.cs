using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;


public class SpawnScript : MonoBehaviour
{
    // Start is called before the first frame update
   
    [SerializeField] private GameObject meteo;
    [SerializeField] private ParticleSystem lightningParticles;
    float timer;
    void Start()
    {
        timer = Random.Range(3, 10);
        StartCoroutine(CountTime());
    }

    // Update is called once per frame

    void Spawn()
    {
        Instantiate(meteo, transform.position, Quaternion.identity);
        timer = Random.Range(3, 10);
        
        
    }


    private IEnumerator CountTime()
    {
        while (GameTimer.Currenttime >= 0)
        {
           
            yield return new WaitForSeconds(1f);
            if (GameTimer.Currenttime % timer == 0)
                Spawn();
            
        }
    }
}
