using System;
using System.Net.Mime;
using UnityEngine;
using TMPro;
using Unity.Mathematics;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Manager
{
    public class ItemManager : MonoBehaviour
    {
        [SerializeField] private GameObject Landmine;
        public int numLandmine;
        public Image Landmine_Pic;
        //public UnityEvent ShowLandmine;

        [SerializeField] TextMeshProUGUI textLandmine;

        private void Update()
        {
            LandmineFunc();
        }

        void LandmineFunc()
        {
            if (numLandmine == 0)
            {
                Landmine_Pic.gameObject.SetActive(false);
            }
            if (numLandmine > 0)
            {
                Landmine_Pic.gameObject.SetActive(true);
            }
            if (Input.GetKeyDown(KeyCode.F)&& numLandmine!=0)
            {
                Pae p = FindObjectOfType<Pae>();
                GameObject go = Instantiate(Landmine, p.transform.position, Quaternion.identity); 
                numLandmine--;
            }
            textLandmine.text = numLandmine.ToString();
           
        }
    }
}