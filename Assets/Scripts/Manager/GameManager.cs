using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Recorder;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    
    [SerializeField] private Pae player; 
    [SerializeField] private GameTimer gameTimer;
    [SerializeField] private Enemy_Melee _melee;
    [SerializeField] private Enemy_Shoot _shoot;
    [SerializeField] private TextMeshProUGUI Text;
    public static int Wave = 1;
    private float HighScore;
    
   
    private GameState _currentState;
    public GameState currentState
    {
        get
        {
            return _currentState;
        }
    }
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        _currentState = GameState.Default;
    }
    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManagerOnsceneLoaded; 
    }
    
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManagerOnsceneLoaded; 
    }

    private void SceneManagerOnsceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name is "DupScene")
        {
            gameTimer.ResetTime();
        }
    }

    void Update()
    {
       
        if (player != null && player.Health <= 0)
        {
            HandlePlayerDeath();
        }
        
    }

    private void HandlePlayerDeath()
    {
        // หยุดการจับเวลา
        gameTimer.Stop();
        HighScore = GameTimer.Currenttime;
        _melee.Stop();
        _shoot.Stop();
        SceneManager.LoadScene("Menu");
    }
}
