using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonEventManager : MonoBehaviour
{
    public static ButtonEventManager Instance;
    public Animator transitionAnimator;


    private void Awake()
    {
        // if (!Instance)
        // {
        //     Instance = this;
        //     DontDestroyOnLoad(this);
        // }
        // else
        // {
        //     Destroy(this);
        // }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManagerOnsceneLoaded;
    }
    
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManagerOnsceneLoaded;
    }

    private void SceneManagerOnsceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Here, call when scene changed
        // if (!transitionAnimator)
        //     transitionAnimator = GameObject.FindWithTag("Transition").GetComponent<Animator>();
    }

    private void Start()
    {
        // if (!transitionAnimator)
        //     transitionAnimator = GameObject.FindWithTag("Transition").GetComponent<Animator>();
    }

    public IEnumerator LoadScene(string name)
    {
        //trigger animation
        transitionAnimator.SetTrigger("Start");
        yield return new WaitForSeconds(1.5f);
        //Load scene
        SceneManager.LoadScene(name);
    }

    public void ButtonLoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void ExitProgram()
    {
        Application.Quit();
    }
}