using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameTimer : MonoBehaviour
{
    private static float currenttime;
    [SerializeField]private TextMeshProUGUI text;

    public static float Currenttime
    {
        get => currenttime;
        
    }

    void Start()
    {
        currenttime += 1;
        StartCoroutine(CountTime());
    }

    private void Update()
    {
        text.text ="Time : " + currenttime.ToString();
        
            
    }

    private void OnDisable()
    {
        StopCoroutine(CountTime());
    }
    public void ResetTime()
    {
        currenttime = 0;
    }
    public void Stop()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    private IEnumerator CountTime()
    {
        while (currenttime >= 0)
        {
            yield return new WaitForSeconds(1f);
            currenttime += 1;
            if (currenttime % 60 == 0)
            {
                GameManager.Wave += 1;
            }
        }
        
    }

    

  
}
