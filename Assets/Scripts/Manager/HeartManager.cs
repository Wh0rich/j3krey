using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HeartManager : MonoBehaviour
{
    [SerializeField] private Image[] Heart;
    [SerializeField] private Sprite Fullheart;
    private Pae p;
    private int _maxHeart = 3;
    private int _heart;

    private void Start()
    {
        p = FindObjectOfType<Pae>();
        _heart = p.Health;
        for (int i = 0; i < _heart; i++)
        {
            Heart[i].sprite = Fullheart;
        }
    }

    void Update()
    {
        p = FindObjectOfType<Pae>();
        _heart = p.Health;
        
        for (int i = _heart; i < _maxHeart; _maxHeart--)
        {
            Heart[i].enabled = false ;
            break;
        }
    }
}
