using System;
using Manager;
using UnityEngine;
using UnityEngine.Events;

namespace Items
{
    public class Landmine_Item : MonoBehaviour
    {
        private ItemManager item;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                item = FindObjectOfType<ItemManager>();
                item.numLandmine += 1;
                Destroy(gameObject);
            }
        }
    }
}