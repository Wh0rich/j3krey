using UnityEngine;

namespace Items
{
    public class SpinMovmement : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private float _angularSpeed = 5.0f;
        [SerializeField] private Vector3 _axisOfRotation ;
        private Transform _objTransformComponent;

        void Start() 
        {
            _axisOfRotation = new Vector3(0, 0, 1);
            _objTransformComponent = this.gameObject.GetComponent<Transform>();
        }

        // Update is called once per frame
        void Update()
        {
            _objTransformComponent.Rotate(_axisOfRotation, _angularSpeed);
        }
    }


}