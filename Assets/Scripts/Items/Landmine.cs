using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Landmine : MonoBehaviour
{
    //private GameObject LandminePrefab;
    public UnityEvent ChangeState;
    private float intervalTime;
    private bool BombActive;
    void Start()
    {
        StartCoroutine(SetBomb(3));
    }
    
    IEnumerator SetBomb(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ChangeState.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Enemy"))
        {
            Explode();
        }
    }
    
    protected void Explode()
    {
        float m_ImpulsePower = 50;
        float m_Radius = 5f;
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, m_Radius);
        foreach (Collider nearbyObject  in colliders)
        {
            if (nearbyObject.CompareTag("Player"))
            {
                var player = FindObjectOfType<Pae>();
                player.Health -= 1;
            }
            if (nearbyObject.CompareTag("Enemy"))
            {
                Destroy(nearbyObject.gameObject);
            }
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(m_ImpulsePower, transform.position, m_Radius);
            }
        }
        Destroy(gameObject);
    }
}
