using System;
using Manager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
namespace Items
{
    public class Goal : MonoBehaviour
    {
        private ItemManager item;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                SceneManager.LoadScene("Menu");
            }
        }
    }
}