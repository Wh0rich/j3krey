using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class SpeedBoost : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pae player = FindObjectOfType<Pae>();
            player.SetSpeed(1600);
            Destroy(gameObject);
        }
    }
}
