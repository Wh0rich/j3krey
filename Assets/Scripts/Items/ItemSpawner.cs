using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField]private GameObject[] Spawner;
    [SerializeField] private GameObject SpeedPrefab;
    [SerializeField] private GameObject LandminePrefab;
    //[SerializeField] private GameObject SpeedPrefab;
    void Start()
    {
        StartCoroutine(SpawnItem());
    }

    IEnumerator SpawnItem()
    {
        if (GameTimer.Currenttime % 15 == 0)
        {
            yield return new WaitForSeconds(0.1f);
            int ran = Random.Range(0, 6);
            int ran2 = Random.Range(0, 6);
            while (ran2 == ran )
            {
                ran2 = Random.Range(0, 6);
            }
            GameObject Speed = Instantiate(SpeedPrefab, Spawner[ran].transform.position, Quaternion.identity);
            GameObject Landmine = Instantiate(LandminePrefab, Spawner[ran2].transform.position, Quaternion.identity);
        }
    }
   
}
