using UnityEngine;
using UnityEngine.Rendering;

public class LightningStrike : MonoBehaviour
{
    
    public ParticleSystem lightningParticles;  
    public float damageAmount = 20f;           
    public float strikeInterval = 5f;         
    public float spawnRadius = 50f;           
    public int numberOfStrikes = 3;            
    public LayerMask playerLayer;
    private float timer;
    
    void Start()
    {
        // Start the lightning strike routine
        InvokeRepeating(nameof(SpawnLightningStrikes), 5f, timer);
      
       
    }

    // Spawns multiple lightning strikes at random positions
    void SpawnLightningStrikes()
    {
            timer = Random.Range(5, 10);
            Vector3 randomPosition = GetRandomPositionWithinRadius();
            // Instantiate the particle system at the random position
            ParticleSystem strikeInstance = Instantiate(lightningParticles, randomPosition, Quaternion.Euler(-90,0,0));
            strikeInstance.Play();
    }

    // Gets a random position within a given radius
    Vector3 GetRandomPositionWithinRadius()
    {
       // Vector2 randomCircle = Random.insideUnitCircle * spawnRadius;
        //Vector3 randomPosition = new Vector3(randomCircle.x, transform.position.y, randomCircle.y);
        Pae player = FindObjectOfType<Pae>();
        Vector3 randomPosition = player.transform.position;
        randomPosition.y = 25f;
        return randomPosition;
    }

    // Performs a raycast to check if the player is hit by lightning
    void PerformRaycastForDamage(Vector3 strikeOrigin)
    {
        
    }

    // For visualization purposes (draws ray in the scene view)
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, spawnRadius);  
    }
}