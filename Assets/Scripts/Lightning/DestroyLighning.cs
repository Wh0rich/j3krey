using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyLighning : MonoBehaviour
{
    // Start is called before the first frame update
    private ParticleSystem particle;
    void Start()
    {
        
    }
    
    private void OnParticleCollision(GameObject other)
    {
        Explode();
        Destroy(gameObject,2);
    }
    protected void Explode()
    {
        float m_ImpulsePower = 50;
        float m_Radius = 3f;
        Vector3 explosionPos = transform.position - new Vector3(0,24.5f,0);
        Collider[] colliders = Physics.OverlapSphere(explosionPos, m_Radius);
        foreach (Collider nearbyObject  in colliders)
        {
            if (nearbyObject.CompareTag("Player"))
            {
                var player = FindObjectOfType<Pae>();
                player.Health -= 1;
            }
            if (nearbyObject.CompareTag("Enemy"))
            {
                Destroy(nearbyObject.gameObject);
            }
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(m_ImpulsePower, transform.position, m_Radius);
            }
        }
    }
}
